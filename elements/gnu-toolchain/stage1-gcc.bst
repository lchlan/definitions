kind: autotools

description:
  Simple cross compiler which is used to build all of stage2.

sources:
- kind: git
  url: upstream:gcc-tarball
  track: baserock/gcc-7.1.0
  ref: 3b0dbcfa2e5d12bd80cab1b35f08653d61fe7838

depends:
- gnu-toolchain/base.bst
- gnu-toolchain/stage1-binutils.bst

variables:
  prefix: /tools
  gcc-arch-flags: ''

  (?):
    # ARM platforms vary a lot even within a single architecture revision,
    # and require tweaks such as this to produce a useful compiler.
    # This is a default configuration that has worked for some targets.
    - arch in ["armv7b", "armv7l"]:
        gcc-arch-flags: --with-arch=armv7-a
    - arch in ["armv7blhf", "armv7lhf"]:
        gcc-arch-flags: >
                        --with-arch=armv7-a
                        --with-cpu=cortex-a9
                        --with-tune=cortex-a9
                        --with-fpu=vfpv3-d16
                        --with-float=hard

environment:
  PATH: /tools/bin:/tools/sbin:/usr/bin:/bin:/usr/sbin:/sbin


config:
  configure-commands:
  # Workaround from LFS due GCC not detecting stack protection correctly
  - |
    sed -i -e '/k prot/a \
    gcc_cv_libc_provides_ssp=yes
    ' gcc/configure

  - mkdir o

  # Configure flag notes:
  #  1. See gcc.morph.
  #  2. Although we will be setting a sysroot at runtime, giving a
  #     temporary one at configuration time seems necessary so that
  #     `--with-native-system-header-dir` produces effect and
  #     /tools/include is in the include path for the newly built GCC. We
  #     set it by default to a non-existent directory to avoid GCC looking
  #     at the host dirs, in case we forget to give it at runtime.
  #  3. Disable searching /usr/local/include for headers
  #  4. The pass 1 compiler needs to find the libraries we build in pass
  #     2.  Include path must be set explicility, because it defaults to
  #     $SYSROOT/usr/include rather than $SYSROOT/include.
  #  5. Disable stuff that doesn't work when building a cross compiler
  #     without an existing libc, and generally try to keep this build as
  #     simple as possible.
  - |

    cd o && ../configure %{gcc-arch-flags}                        \
              --build=$(sh ../config.guess)                       \
              --host=$(sh ../config.guess)                        \
              --target=%{target-stage1}                           \
              --prefix="%{prefix}"                                \
      `# [1]` --libdir="%{libdir}"                                \
      `# [2]` --with-sysroot=/nonexistentdir                      \
              --with-newlib                                       \
      `# [3]` --with-local-prefix="%{prefix}"                     \
      `# [4]` --with-native-system-header-dir="%{prefix}/include" \
              --without-headers                                   \
              --disable-nls                                       \
              --disable-shared                                    \
              --disable-multilib                                  \
      `# [5]` --disable-decimal-float                             \
      `# [5]` --disable-threads                                   \
      `# [5]` --disable-libatomic                                 \
      `# [5]` --disable-libgomp                                   \
      `# [5]` --disable-libitm                                    \
      `# [5]` --disable-libmpx                                    \
      `# [5]` --disable-libquadmath                               \
      `# [5]` --disable-libsanitizer                              \
      `# [5]` --disable-libssp                                    \
      `# [5]` --disable-libvtv                                    \
      `# [5]` --disable-libcilkrts                                \
      `# [5]` --disable-libstdc++-v3                              \
              --enable-languages=c,c++

  build-commands:
  - cd o && make

  install-commands:
  - cd o && make DESTDIR="%{install-root}" install

  (?):
    # GCC is not passing the correct host/target flags to GMP's configure
    # script, which causes it to not use the machine-dependent code for
    # the platform and use the generic one instead.  However, the generic
    # code results on an undefined reference to `__gmpn_invert_limb' in
    # ARMv7.  Fix the invocation of GMP's configure script so that GMP can
    # use the machine-dependent code.
    - arch.startswith("armv5"):
        build-commands:
          (<):
          - sed -i "s/--host=none/--host=armv5/" o/Makefile;
          - sed -i "s/--target=none/--target=armv5/" o/Makefile
    - arch.startswith("armv7"):
        build-commands:
          (<):
          - sed -i "s/--host=none/--host=armv7a/" o/Makefile
          - sed -i "s/--target=none/--target=armv7a/" o/Makefile
